window.onload = function() {
    license_terms = document.querySelectorAll("li");
    
    for (var i = 0; i < license_terms.length; i++) {
        license_terms[i].addEventListener('click', wasSelected)
    }

    document.querySelector("#restart").addEventListener('click', maybeReload)
}

var wasSelected = function(evt) {
    evt.preventDefault();
    var selected = evt.target.classList.contains("selected");

    if (!selected) {
        evt.target.classList.add("selected");
    }
    else {
        evt.target.classList.remove("selected");
    }
}

var maybeReload = function(evt) {
    var restart = confirm("Do you want to restart?");
    if(restart) {
        location.reload();
    }
}

var fakeLicense = [
    {message: "The USER relinquishes the right to their soul", correct: true},
    {message: "The USER agrees that any and all liability falls on them and not us", correct: true},
    {message: "The USER allows us to collect any and all data", correct: false},
	  {message: "The USER allows us to collect the USER's location at all times", correct: true},
	  {message: "The COMPANY can make phone calls to the USER at any time of day (or night!)", correct: false},
	  {message: "The USER agrees to pay at least $500 to the COMPANY at the end of every fiscal year", correct: false},
	  {message: "The COMPANY can upcharge the USER at any point during the subscription without notifying the USER", correct: true},
	  {message: "The COMPANY can charge the USER twice in a fiscal year without notifying the user", correct: true},
	  {message: "The USER cannot recieve a refund for any amount paid to the COMPANY during any point of the fiscal year", correct: true},
	  {message: "If the USER sues the COMPANY at any point, the USER will pay 20% of any lawsuit/attorney charges", correct: true},
	  {message: "By using this application, the USER agrees to let the COMPANY come to their house for dinner at any time", correct: false},
	  {message: "The USER has the right to challenge the COMPANY in any matter concerning this agreement", correct: false},
	  {message: "The COMPANY is allowed to stop notifying the USER of incoming payments if that user has been subscribed for more than 1 year", correct: true},
	  {message: "If the USER fails to pay the subscription fee within 48 hours of its due date, the USER must eat an entire bowl of 2 week old clam chowder", correct: false},
	  {message: "The COMPANY reserves the right to forcibly employ the first born of the USER", correct: true},
	  {message: "The COMPANY reserves the right to cancel the USER's subscription service while continuing to charge them until the fiscal year has ended", correct: false},
	  {message: "If the USER cancels the service, the COMPANY reserves the right to end the life of the USER immediately", correct: true},
	  {message: "The USER agrees to pay any electric bill that may come over the COMPANY during the lifetime of the USER", correct: false},
	  {message: "Any HDMI cables owned by the USER may be confiscated by the COMPANY at any point during the subscription period of the USER", correct: true},
	  {message: "The COMPANY is not liable for any physical harm they may cause to the USER at any point", correct: true},
	  {message: "Every time the Giants win, the USER will be punched in the face by an employee of the COMPANY", correct: false},
	  {message: "Anytime the USER calls the COMPANY for tech support, the COMPANY may charge them a minimum fee of $5.00 per minute", correct: true},
	  {message: "The USER is not allowed to subscribe to any services the competitors offer, even after cancellation of subscription", correct: true},
	  {message: "If the USER does not answer their phone when the COMPANY is calling, the COMPANY has permission to find them and kill them", correct: false},
	  {message: "The USER relinquishes all rights to privacy, including bathroom privacy", correct: false},
	  {message: "The USER must buy the CEO of the COMPANY a Christmas Present every year until the USER has perished", correct: true},
	  {message: "If the COMPANY asks the USER to participate in a dance competition, the USER must oblige", correct: true},
	  {message: "The COMPANY agrees to allow anyone access to any information the USER has ever given the COMPANY", correct: false},
	  {message: "Medieval Torture is discouraged, but not entirely forbidden, in the punishment of the USER", correct: true},
]

var vm = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        contentPresent: true,
        content: fakeLicense,
        score: 0,
        selected: [],
    },
    methods: {
        updateScore: function(inputLine) {
            let line = fakeLicense.find(o => o.message === inputLine);
            if (!vm.selected.find(o => o.message === inputLine)) {
                vm.selected.push(line);
                line.correct ? vm.score++ : vm.score--;
            }
            else {
                var index = vm.selected.indexOf(line);
                vm.selected.splice(index, 1);
                line.correct ? vm.score-- : vm.score++;
            }
        },
        showScore: function() {
            document.querySelector("#score").style.display = "block";
        }
    }
})
